## Start

Just ```docker-compose up -d```

After build , visit ```http://[IP]:9898``` (change [IP] to your ip or domain)

## Description & API

```
Resources
https://jsonplaceholder.typicode.com/posts	100 posts
https://jsonplaceholder.typicode.com/comments	500 comments
https://jsonplaceholder.typicode.com/users	10 users

Routes
GET	/posts
GET	/posts/1
GET	/posts/1/comments
GET	/comments?postId=1
```

## Features

[x] The post list is scrollable, like Facebook.

[x] Support the feature of switching account ( From user1 to user10).

[x] (Optional) Comments can be displayed in the post.

[ ] (Optional) You can reply comment in each comment in each post.


## Technical Requirements

[x] **SSR with React.** without any SSR framework


## Nice to have

[x] Provide Docker file

[x] Use Webpack instead of create-react-app

[x] Use styled-component

[x] Use Redux