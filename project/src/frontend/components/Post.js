import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { fetchSinglePost } from "../redux/posts/posts.actions";
import { Link } from "react-router-dom";
import ui from '../ui';

let postId

const Post = (props) => {

  const dispatch = useDispatch();
  let post = useSelector((state) => { return state.posts.post });
  let comments = useSelector((state) => { return state.posts.comments });
  postId = props.match.params.postId || false

  useEffect(() => {
    dispatch(fetchSinglePost(postId));
  }, []);
  return (
    <div className="entry">
      <Link to="/">Back To List</Link>
      <ui.sectionTitle>Post</ui.sectionTitle>
      <div className="post-postId">Post id: {post.id}</div>
      <div className="post-userId">User id: {post.userId}</div>
      <div className="post-title">Title: {post.title}</div>
      <div className="post-body">Body: {post.body}</div>
      <ui.sectionTitle>Comments</ui.sectionTitle>
      {comments.map((comment) => (
        <ui.commentItem key={comment.id}>
          <p>Name: {comment.name}</p>
          <p>Email: {comment.email}</p>
          <p>Body: {comment.body}</p>
        </ui.commentItem>
      ))}
    </div>
  );
};

Post.getInitialData = async (store) => {
  return store.dispatch(fetchSinglePost(postId));
}

export default Post;