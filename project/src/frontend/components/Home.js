import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { fetchPosts } from "../redux/posts/posts.actions";
import ui from '../ui'

let userId = false
let start = 0
let limit = 20

const Home = (props) => {
  const dispatch = useDispatch();
  let posts = useSelector((state) => { return state.posts.posts });

  let switchUserPost = (s) => {
    start = 0
    dispatch(fetchPosts(start, limit, s));
  }


  useEffect(() => {

    window.addEventListener('scroll', (event) => {
      let documentBody = document.body
      let doInfinityLoad = (documentBody.scrollTop + documentBody.clientHeight) >= documentBody.scrollHeight
      if (doInfinityLoad) {
        start += 20
        dispatch(fetchPosts(start, limit, userId));
      }
    });

    dispatch(fetchPosts(start, limit, userId));
  }, []);

  return (
    <div>
      <ui.sectionTitle>Homepage</ui.sectionTitle>
      <ui.userList>
        <span>Switch user's post:</span>
        <ui.userListItem onClick={() => { switchUserPost(false) }}>All</ui.userListItem>
        {[1,2,3,4,5,6,7,8,9,10].map((el) => {
          return <ui.userListItem key={el} onClick={() => { switchUserPost(el) }}>User{el}</ui.userListItem>
        })}
      </ui.userList>

      <ui.postList>
          {posts.map((post) => (
            <ui.postListItem key={post.id}>
              <h3>Post id: {post.id}</h3>
              <h3>Title: <ui.postListItemLink to={'/post/' + post.id}>{post.title}</ui.postListItemLink> </h3>
            </ui.postListItem>
          ))}
      </ui.postList>
    </div>
  );
};

Home.getInitialData = async (store) => {
  return store.dispatch(fetchPosts(start, limit, userId));
}

export default Home;