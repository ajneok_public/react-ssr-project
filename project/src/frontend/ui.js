import styled from 'styled-components';

import { Link } from "react-router-dom";

let green1 = '#023535';
let green2 = '#015958';
let green3 = '#008F8C';
let green4 = '#0CABA8';
let green5 = '#0FC2C0';

let sectionTitle = styled.h1`
    color: ${green2};
    margin: 0;
`;

let userList = styled.div`
    padding-top: 14px;
    padding-bottom: 14px;
`;
let userListItem = styled.div`
    color: ${green3};
    cursor: pointer;
    padding: 4px;
    border: 1px solid ${green3};
    margin-left: 6px;
    display: inline-block;
    border-radius: 3px;
`;

let postList = styled.div``
let postListItem = styled.div`
    margin-top: 14px;
    border-top: 1px solid ${green1}
`
let postListItemLink = styled(Link)`
    color: ${green1};
    font-weight: 600;
`

let commentItem = styled.div`
    margin-top: 14px;
    padding-left: 6px;
    border-left: 3px solid ${green2};
`
export default {
    sectionTitle,
    userList,
    userListItem,
    postList,
    postListItem,
    postListItemLink,
    commentItem
 }