import React from "react";
import { createRoot } from 'react-dom/client';

import { BrowserRouter } from "react-router-dom";
import Routes from "./routes";
import { renderRoutes } from "react-router-config";

import { Provider } from "react-redux";
import store from "./redux";
import "./style.scss";

const container = document.getElementById('root');
const root = createRoot(container);

root.render(
    <Provider store={store}>
        <BrowserRouter>
            {renderRoutes(Routes)}
        </BrowserRouter>
    </Provider>
);
