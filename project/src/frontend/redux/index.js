import { configureStore } from '@reduxjs/toolkit'
import postsReducer from "./posts/posts.reducer";

let reducer = {
    posts: postsReducer
}
const store = configureStore({ reducer });
export default store;