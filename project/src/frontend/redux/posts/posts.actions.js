import axios from "axios";

export const FETCH_POSTS = "FETCH_POSTS";
export const FETCH_SINGLE_POST = "FETCH_SINGLE_POST";

const isLoading = false

export const fetchPosts = (start, limit, userId) => async (dispatch) => {
    let fetchUrl = 'https://jsonplaceholder.typicode.com/posts?'
    let query = []
    let infinity = start !== 0
    if (start || start === 0) { query.push(`_start=${start}`) }
    if (limit) { query.push(`_limit=${limit}`) }
    if (userId) { query.push(`userId=${userId}`) }

    fetchUrl += query.join('&')

    const res = await axios.get(fetchUrl);

    dispatch({
        type: FETCH_POSTS,
        payload: res.data,
        infinity: infinity
    });
};

export const fetchSinglePost = (postId) => async (dispatch) => {
    let post = {}
    let comments = []
    if (postId) {
        post = await axios.get(`https://jsonplaceholder.typicode.com/posts/${postId}`);
        comments = await axios.get(`https://jsonplaceholder.typicode.com/posts/${postId}/comments`);
    }
    
    dispatch({
        type: FETCH_SINGLE_POST,
        payload: {
            post: post.data,
            comments: comments.data
        },
    });
};