import { FETCH_POSTS, FETCH_SINGLE_POST } from "./posts.actions";

const reducer = (state = { posts: [], post: {}, comments: [] }, action) => {
    switch (action.type) {
        case FETCH_POSTS:
            let posts = action.infinity ? state.posts.concat(action.payload) : action.payload
            return {
                ...state,
                posts: posts
            };
        case FETCH_SINGLE_POST:
            return {
                ...state,
                post: action.payload.post,
                comments: action.payload.comments,
            };
        default:
            return state;
    }
};

export default reducer;