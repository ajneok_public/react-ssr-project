import Home from "./components/Home";
import Post from "./components/Post";

const Routes = [
  {
    exact: true,
    path: "/",
    component: Home
  },
  {
    path: "/post/:postId",
    component: Post
  }
];

export default Routes;