import express from "express";
import myRoutes from "./frontend/routes";
import store from "./frontend/redux";

import { matchRoutes } from "react-router-config";
import render from "./renderHelper";


const app = express();

app.use(express.static("public"));


app.get("*", (req, res) => {

  let promises = matchRoutes(myRoutes, req.path).map(({ route }) => {
    let component = route.component;
    return component.getInitialData ? component.getInitialData(store) : null;
  });

  Promise.all(promises).then(() => {
    let html = render(req, store);
    res.send(html);
  });

});

app.listen(9898, () => {
  console.log("listening on port 9898");
});